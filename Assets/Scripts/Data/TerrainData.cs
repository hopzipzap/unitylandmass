﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu()]
public class TerrainData : UpdateData
{

    public float uniformScale = 2.5f;

    public bool useFalloff;
    public bool useFS;

    public float meshHeightMultiplier;
    public AnimationCurve meshHeightCurve;
}
