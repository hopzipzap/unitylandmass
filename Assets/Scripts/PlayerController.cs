﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public Animator anim;
    private float inputH;
    private float inputV;
    private bool run;
    private bool aim;
    private bool shoot;
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        shoot = false;
        if (Input.GetMouseButtonDown(0) && (aim = true))
        {
            shoot = true;
        }
        if (Input.GetMouseButton(1))
        {
            aim = true;
        } 
        else
        {
            aim = false;
        }
        if (Input.GetKey(KeyCode.LeftShift))
        {
            run = true;
        }
        else
        {
            run = false;
        }
        anim.SetBool("Jump", false);
        if (Input.GetKeyDown(KeyCode.Space))
        {
            anim.SetBool("Jump", true);
        }
        inputH = Input.GetAxis("Horizontal");
        inputV = Input.GetAxis("Vertical");
        anim.SetBool("Shoot", shoot);
        anim.SetBool("Aim", aim);
        anim.SetFloat("InputH", inputH);
        anim.SetFloat("InputV", inputV);
        anim.SetBool("Run", run);
        float h = Input.GetAxisRaw("Horizontal");
        float v = Input.GetAxisRaw("Vertical");

    }
}
