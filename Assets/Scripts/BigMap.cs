﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BigMap : MonoBehaviour {

    #region Variables Main var
    const float viewerPieceUpdate = 25f;
	const float viewerPieceUpdateSqr = viewerPieceUpdate * viewerPieceUpdate;

	public LODInfo[] lod;
	public static float maxViewDst;

	public Transform viewer;
	public Material mapMaterial;

	public static Vector2 viewerPosition;
	Vector2 viewerPositionOld;
	static MapGenerator mapGenerator;
	int pieceSize;
	int visiblePieceViewed;

	Dictionary<Vector2, PieceOfMap> pieceDictionary = new Dictionary<Vector2, PieceOfMap>();
	static List<PieceOfMap> lastPieceUpdated = new List<PieceOfMap>();
    #endregion
    #region unity funcs
    void Start()
    { 
		mapGenerator = FindObjectOfType<MapGenerator> ();

		maxViewDst = lod[lod.Length - 1].visibleDstThreshold;
		pieceSize = MapGenerator.mapPieceSize - 1;
		visiblePieceViewed = Mathf.RoundToInt(maxViewDst / pieceSize);

		VisiblePieceUpdate ();
	}

	void Update() 
    {
		viewerPosition = new Vector2 (viewer.position.x, viewer.position.z) / mapGenerator.terrainData.uniformScale;

		if ((viewerPositionOld - viewerPosition).sqrMagnitude > viewerPieceUpdateSqr)
        {
			viewerPositionOld = viewerPosition;
			VisiblePieceUpdate ();
		}
	}
    #endregion
#region functions && methods and structs
    void VisiblePieceUpdate()
    {

		for (int i = 0; i < lastPieceUpdated.Count; i++) 
        {
			lastPieceUpdated [i].SetVisible (false);
		}
		lastPieceUpdated.Clear ();
			
		int currPieceX = Mathf.RoundToInt (viewerPosition.x / pieceSize);
		int currPieceY = Mathf.RoundToInt (viewerPosition.y / pieceSize);

		for (int yOffset = -visiblePieceViewed; yOffset <= visiblePieceViewed; yOffset++) 
        {
			for (int xOffset = -visiblePieceViewed; xOffset <= visiblePieceViewed; xOffset++)
            {
				Vector2 XYviewed = new Vector2 (currPieceX + xOffset, currPieceY + yOffset);

				if (pieceDictionary.ContainsKey (XYviewed)) {
					pieceDictionary [XYviewed].TerrainPieceUpdate ();
				} else {
					pieceDictionary.Add (XYviewed, new PieceOfMap (XYviewed, pieceSize, lod, transform, mapMaterial));
				}

			}
		}
	}

	public class PieceOfMap {

		GameObject meshObject;
		Vector2 position;
		Bounds bounds;

		MeshRenderer meshRenderer;
		MeshFilter meshFilter;
        MeshCollider meshCollider;
		LODInfo[] lod;
		LODMesh[] lodMeshes;
        LODMesh collisionLODMesh;
		MapData mapData;
		bool mapDataReceived;
		int previousLODIndex = -1;

		public PieceOfMap(Vector2 coord, int size, LODInfo[] lod, Transform parent, Material material) 
        {
            #region POM vars
            this.lod = lod;

			position = coord * size;
			bounds = new Bounds(position,Vector2.one * size);
			Vector3 positionV3 = new Vector3(position.x,0,position.y);

			meshObject = new GameObject("Piece of Map");
			meshRenderer = meshObject.AddComponent<MeshRenderer>();
			meshFilter = meshObject.AddComponent<MeshFilter>();
            meshCollider = meshObject.AddComponent<MeshCollider>();
			meshRenderer.material = material;

			meshObject.transform.position = positionV3 * mapGenerator.terrainData.uniformScale;
			meshObject.transform.parent = parent;
			meshObject.transform.localScale = Vector3.one * mapGenerator.terrainData.uniformScale;
			SetVisible(false);

			lodMeshes = new LODMesh[lod.Length];
            #endregion
            for (int i = 0; i < lod.Length; i++) 
            {
				lodMeshes[i] = new LODMesh(lod[i].lod, TerrainPieceUpdate);
                if (lod[i].useForCollider)
                {
                    collisionLODMesh = lodMeshes[i];
                }
			}

			mapGenerator.RequestMapData(position,OnMapDataReceived);
		}

		void OnMapDataReceived(MapData mapData) 
        {
			this.mapData = mapData;
			mapDataReceived = true;

			Texture2D texture = TextureGenerator.TextureFromColorMap (mapData.colorMap, MapGenerator.mapPieceSize, MapGenerator.mapPieceSize);
			meshRenderer.material.mainTexture = texture;

			TerrainPieceUpdate ();
		}

	

		public void TerrainPieceUpdate() 
        {
			if (mapDataReceived) 
            {
				float viewerDstFromNearestEdge = Mathf.Sqrt (bounds.SqrDistance (viewerPosition));
				bool visible = viewerDstFromNearestEdge <= maxViewDst;

				if (visible) 
                {
					int lodIndex = 0;

					for (int i = 0; i < lod.Length - 1; i++)
                    {
						if (viewerDstFromNearestEdge > lod [i].visibleDstThreshold) {
							lodIndex = i + 1;
						} else
                        {
							break;
						}
					}
                        
					if (lodIndex != previousLODIndex)
                    {
						LODMesh lodMesh = lodMeshes [lodIndex];
						if (lodMesh.hasMesh) {
							previousLODIndex = lodIndex;
							meshFilter.mesh = lodMesh.mesh;
                            
						} else if (!lodMesh.hasRequestedMesh) 
                        {
							lodMesh.RequestMesh (mapData);
						}
                        if (lodIndex == 0)
                        {
                            if (collisionLODMesh.hasMesh)
                            {
                                meshCollider.sharedMesh = collisionLODMesh.mesh;
                            }
                            else if (!collisionLODMesh.hasRequestedMesh)
                            {
                                collisionLODMesh.RequestMesh(mapData);
                            }
                        }
					}

					lastPieceUpdated.Add (this);
				}

				SetVisible (visible);
			}
		}

		public void SetVisible(bool visible) 
        {
			meshObject.SetActive (visible);
		}

		public bool IsVisible() 
        {
			return meshObject.activeSelf;
		}

	}

	class LODMesh
    {

		public Mesh mesh;
		public bool hasRequestedMesh;
		public bool hasMesh;
		int lod;
		System.Action updateCallback;

		public LODMesh(int lod, System.Action updateCallback) 
        {
			this.lod = lod;
			this.updateCallback = updateCallback;
		}

		void OnMeshDataReceived(MeshData meshData) 
        {
			mesh = meshData.CreateMesh ();
			hasMesh = true;

			updateCallback ();
		}

		public void RequestMesh(MapData mapData) 
        {
			hasRequestedMesh = true;
			mapGenerator.RequestMeshData (mapData, lod, OnMeshDataReceived);
		}

	}

	[System.Serializable]
	public struct LODInfo 
    {
		public int lod;
		public float visibleDstThreshold;
        public bool useForCollider;
	}

}
#endregion