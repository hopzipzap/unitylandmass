﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bow : MonoBehaviour
{
#region vars
    public Animator animator;
    public float tension;
    private bool _pressed;
    public Transform ropePos;
    public Vector3 ropeNearLocalPos;
    public Vector3 ropeFarLocalPos;
    public AnimationCurve ropeDrawAnimation;
    public AnimationCurve ropeReturnAnimation;
    public float returnSpeed;
    public Arrow CurrentArrow;
    public float ArrowSpeed;
    public AudioSource BowTensionSound;
    public AudioSource ArrowWhistleSound;
    public Arrow[] Arrows;
    private int ArrowIndex = 0;
    #endregion
    // Start is called before the first frame update
    void Start()
    {
        ropeNearLocalPos = ropePos.localPosition;
    }

    // Update is called once per frame
    void Update()
    {
        /*if (Input.GetMouseButtonDown(0))
        {
            _pressed = false;
            
            StartCoroutine(RopeReturn());
            tension = 0;


        }*/
        if (Input.GetMouseButtonDown(1))
        {
            
            _pressed = true;
            ArrowIndex++;
            if (ArrowIndex >= Arrows.Length)
                ArrowIndex = 0;
            CurrentArrow = Arrows[ArrowIndex];
            CurrentArrow.SetToRope(ropePos);
            BowTensionSound.pitch = Random.Range(0.8f, 1.2f);
            BowTensionSound.Play();
        }
        if (Input.GetMouseButtonUp(1))
        {
            _pressed = false; 
            
            CurrentArrow.Shot(ArrowSpeed * tension * 3f);
            tension = 0;
            StartCoroutine(RopeReturn());
            BowTensionSound.Stop();
            ArrowWhistleSound.pitch = Random.Range(0.8f, 1.2f);
            ArrowWhistleSound.Play();
        }
        if (_pressed && !isPlaying(animator, "Standing Draw Arrow") && !isPlayingRecoil(animator, "Standing Aim Recoil"))
        {
            if (tension < 1f)
            {
                tension += Time.deltaTime;
            }
            ropePos.localPosition = Vector3.LerpUnclamped(ropeNearLocalPos, ropeFarLocalPos, ropeDrawAnimation.Evaluate(tension));
        }
       
    }

    bool isPlaying(Animator anim, string stateName)
    {
        if (anim.GetCurrentAnimatorStateInfo(0).IsName(stateName) &&
                anim.GetCurrentAnimatorStateInfo(0).normalizedTime < 0.5f)
            return true;
        else
            return false;
    }

    bool isPlayingRecoil(Animator anim, string stateName)
    {
        if (anim.GetCurrentAnimatorStateInfo(0).IsName(stateName) &&
                anim.GetCurrentAnimatorStateInfo(0).normalizedTime < 1f)
            return true;
        else
            return false;
    }

    IEnumerator RopeReturn()
    {
        Vector3 startLocalPos = ropePos.localPosition;
        for (float f = 0; f < 1f; f+=Time.deltaTime / returnSpeed)
        {
            ropePos.localPosition = Vector3.LerpUnclamped(startLocalPos, ropeNearLocalPos, ropeReturnAnimation.Evaluate(f));
            yield return null;
        }
        
    }
}
