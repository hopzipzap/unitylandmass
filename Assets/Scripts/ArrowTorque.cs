﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowTorque : MonoBehaviour
{
    public Rigidbody rb;
    public float velocityValue;
    public float angularVelocityMult;
    
    void FixedUpdate()
    {
        Vector3 cross = Vector3.Cross(transform.forward, rb.velocity.normalized);
        rb.AddTorque(cross * rb.velocity.magnitude * velocityValue);
        rb.AddTorque((-rb.angularVelocity + Vector3.Project(rb.angularVelocity ,transform.forward)) * rb.velocity.magnitude * angularVelocityMult);
    }
}
